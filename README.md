# Money Rest

## A RESTful web service for money transfers between accounts

### How to build and run

To build the application use the following command:
```
[user@localhost money-rest]$ ./gradlew clean build  
```

To just run tests:
```
[user@localhost money-rest]$ ./gradlew clean check  
```

You can run the application using Gradle:
```
[user@localhost money-rest]$ ./gradlew clean run  
```

Or you can build it and then run from your command line:
```
[user@localhost money-rest]$ ./gradlew clean build
[user@localhost money-rest]$ java -jar build/libs/money-rest-1.0.0-SNAPSHOT-all.jar 
```

To stop the application press *Ctrl+C*.

### API usage

The API is organized around REST. The service has predictable resource-oriented URLs, accepts JSON-encoded request bodies, returns JSON-encoded responses, and uses standard HTTP response codes and verbs. 

### Accounts

#### Create account

This endpoint creates an account.

**Request url:**
```
POST http://localhost:7000/rest/1.0/accounts
```

**Request fields:**

| Field    | Description          | Format                     |
| -------- | -------------------- | -------------------------- |
| currency | the account currency | 3-letter ISO currency code |

The service returns the HTTP `201 Created`  status code, if the request has succeeded.

**Response fields:**

| Field    | Description                                            | Format                     |
| -------- | ------------------------------------------------------ | -------------------------- |
| id       | the account ID                                         | UUID                       |
| balance  | the available balance                                  | Decimal                    |
| currency | the account currency                                   | 3-letter ISO currency code |
| created  | the instant when the account was created               | ISO date and time in UTC   |
| updated  | the instant when the account was updated the last time | ISO date and time in UTC   |

**Example request:**
```
[user@localhost money-rest]$ curl -w "\n" -d '{"currency":"RUB"}' http://localhost:7000/rest/1.0/accounts
```

**Example response:**
```
{
  "id": "82543504-9950-4b46-9a5e-aba0d9cc309d",
  "balance": 0,
  "currency": "RUB",
  "created": "2019-03-10T15:04:46.979053Z",
  "updated": "2019-03-10T15:04:46.979053Z"
}
```

#### Get account

This endpoint returns one of accounts by ID.

**Request url:**
```
GET http://localhost:7000/rest/1.0/accounts/<id>
```

**URL parameters:**

| Parameter | Description                      | Format |
| --------- | -------------------------------- | ------ |
| id        | the ID of the account to request | UUID   |

**Response fields:**

| Field    | Description                                            | Format                     |
| -------- | ------------------------------------------------------ | -------------------------- |
| id       | the account ID                                         | UUID                       |
| balance  | the available balance                                  | Decimal                    |
| currency | the account currency                                   | 3-letter ISO currency code |
| created  | the instant when the account was created               | ISO date and time in UTC   |
| updated  | the instant when the account was updated the last time | ISO date and time in UTC   |

**Example request:**
```
[user@localhost money-rest]$ curl -w "\n" http://localhost:7000/rest/1.0/accounts/82543504-9950-4b46-9a5e-aba0d9cc309d
```

**Example response:**
```
{
  "id": "82543504-9950-4b46-9a5e-aba0d9cc309d",
  "balance": 0,
  "currency": "RUB",
  "created": "2019-03-10T15:04:46.979053Z",
  "updated": "2019-03-10T15:04:46.979053Z"
}
```

#### Get accounts

This endpoint returns all accounts.

**Request url:**
```
GET http://localhost:7000/rest/1.0/accounts
```

**Response fields:**

| Field    | Description  | Format                                                                               |
| -------- | ------------ | ------------------------------------------------------------------------------------ |
| accounts | all accounts | array of account objects (containing all fields from the corresponding request by ID |

**Example request:**
```
[user@localhost money-rest]$ curl -w "\n" http://localhost:7000/rest/1.0/accounts
```

**Example response:**
```
{
  "accounts": [
    {
      "id": "1bde01ce-f753-4bd6-8f3b-2a0029c77f9f",
      "balance": 0,
      "currency": "RUB",
      "created": "2019-03-10T15:24:54.096195Z",
      "updated": "2019-03-10T15:24:54.096195Z"
    },
    {
      "id": "0f266ebf-a3b2-4f32-8806-d91baf99f953",
      "currency": "JPY",
      "balance": 0,
      "created": "2019-03-10T15:25:00.668404Z",
      "updated": "2019-03-10T15:25:00.668404Z"
    }
  ]
}
```

#### Delete account

This endpoint deletes one of accounts by ID.

**Request url:**
```
DELETE http://localhost:7000/rest/1.0/accounts/<id>
```

**URL parameters:**

| Parameter | Description                     | Format |
| --------- | ------------------------------- | ------ |
| id        | the ID of the account to delete | UUID   |

The service returns the HTTP `204 No Content` status code, if the request has succeeded.

**Example request:**
```
[user@localhost money-rest]$ curl -w "\n" -X DELETE http://localhost:7000/rest/1.0/accounts/1bde01ce-f753-4bd6-8f3b-2a0029c77f9f
```

### Transactions

#### Create deposit

This endpoint creates a deposit to an account.

**Request url:**
```
POST http://localhost:7000/rest/1.0/transactions/deposit
```

**Request fields:**

| Field    | Description                                                        | Format                     |
| -------- | ------------------------------------------------------------------ | -------------------------- |
| target   | the ID of a target account                                         | UUID                       |
| amount   | the deposit amount                                                 | Decimal                    |
| currency | the deposit currency (a target account should be in this currency) | 3-letter ISO currency code |

The service returns the HTTP `201 Created`  status code, if the request has succeeded.

**Response fields:**

| Field   | Description                                                         | Format                   |
| ------- | ------------------------------------------------------------------- | ------------------------ |
| id      | the ID of the created transaction                                   | UUID                     |
| status  | the transaction status, one of: `COMPLETED`, `DECLINED` or `FAILED` | Text                     |
| created | the instant when the transaction was created                        | ISO date and time in UTC |
| message | the reason explanation, for `DECLINED` or `FAILED` transactions     | Text                     |

**Example request:**
```
[user@localhost money-rest]$ curl -w "\n" -d '{"target":"f34f2ace-c905-4107-aef8-3a018bd0ed47", "amount":1000, "currency":"JPY"}' http://localhost:7000/rest/1.0/transactions/deposit
```

**Example response:**
```
{
  "id": "af8efd9f-fc15-46a4-912e-e2604288393e",
  "status": "COMPLETED",
  "created": "2019-03-10T16:14:20.586698Z",
  "message": null
}

```

#### Create withdrawal

This endpoint creates a withdrawal from an account.

**Request url:**
```
POST http://localhost:7000/rest/1.0/transactions/withdrawal
```

**Request fields:**

| Field    | Description                                                           | Format                     |
| -------- | --------------------------------------------------------------------- | -------------------------- |
| source   | the ID of a source account                                            | UUID                       |
| amount   | the withdrawal amount                                                 | Decimal                    |
| currency | the withdrawal currency (a source account should be in this currency) | 3-letter ISO currency code |

The service returns the HTTP `201 Created`  status code, if the request has succeeded.

**Response fields:**

| Field   | Description                                                         | Format                   |
| ------- | ------------------------------------------------------------------- | ------------------------ |
| id      | the ID of the created transaction                                   | UUID                     |
| status  | the transaction status, one of: `COMPLETED`, `DECLINED` or `FAILED` | Text                     |
| created | the instant when the transaction was created                        | ISO date and time in UTC |
| message | the reason explanation, for `DECLINED` or `FAILED` transactions     | Text                     |

**Example request:**
```
[user@localhost money-rest]$ curl -w "\n" -d '{"source":"3c1b0d7d-bb77-441c-8697-564d7967536e", "amount":1000, "currency":"JPY"}' http://localhost:7000/rest/1.0/transactions/withdrawal
```

**Example response:**
```
{
  "id": "d1f83c78-3419-4566-b025-261ff91c1db1",
  "status": "COMPLETED",
  "created": "2019-03-10T16:29:46.629100Z",
  "message": null
}

```

#### Create transfer

This endpoint create a transfer between accounts.

**Request url:**
```
POST http://localhost:7000/rest/1.0/transactions/transfer
```

**Request fields:**

| Field    | Description                                                                        | Format                     |
| -------- | ---------------------------------------------------------------------------------- | -------------------------- |
| source   | the ID of a source account                                                         | UUID                       |
| target   | the ID of a target account                                                         | UUID                       |
| amount   | the transfer amount                                                                | Decimal                    |
| currency | the transfer currency (both source and target accounts should be in this currency) | 3-letter ISO currency code |

The service returns the HTTP `201 Created`  status code, if the request has succeeded.

**Response fields:**

| Field   | Description                                                         | Format                   |
| ------- | ------------------------------------------------------------------- | ------------------------ |
| id      | the ID of the created transaction                                   | UUID                     |
| status  | the transaction status, one of: `COMPLETED`, `DECLINED` or `FAILED` | Text                     |
| created | the instant when the transaction was created                        | ISO date and time in UTC |
| message | the reason explanation, for `DECLINED` or `FAILED` transactions     | Text                     |

**Example request:**
```
[user@localhost money-rest]$ curl -w "\n" -d '{"source":"3c1b0d7d-bb77-441c-8697-564d7967536e", "target":"f34f2ace-c905-4107-aef8-3a018bd0ed47", "amount":1000, "currency":"JPY"}' http://localhost:7000/rest/1.0/transactions/transfer
```

**Example response:**
```
{
  "id": "8622cfa4-1dbc-4b8e-a1e0-4f7b5def7aad",
  "status": "COMPLETED",
  "created": "2019-03-10T16:34:43.852938Z",
  "message": null
}

```
