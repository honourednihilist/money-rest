package org.bitbucket.honourednihilist.moneyrest;

import com.google.inject.Guice;
import com.google.inject.Injector;

import org.bitbucket.honourednihilist.moneyrest.guice.ConfigModule;
import org.bitbucket.honourednihilist.moneyrest.guice.DbModule;
import org.bitbucket.honourednihilist.moneyrest.guice.RestModule;
import org.bitbucket.honourednihilist.moneyrest.guice.ServiceModule;

import io.javalin.Javalin;

public class Start {

	public static void main(String...args) {
		createInjector().getInstance(Javalin.class).start();
	}

	public static Injector createInjector() {
		return Guice.createInjector(new ConfigModule(), new DbModule(), new ServiceModule(), new RestModule());
	}
}
