package org.bitbucket.honourednihilist.moneyrest.dao;

import org.apache.ibatis.annotations.Param;
import org.bitbucket.honourednihilist.moneyrest.model.Account;

import java.util.List;
import java.util.UUID;

public interface AccountMapper {

	List<Account> getAll();

	Account get(@Param("id") UUID id);

	void create(@Param("account") Account account);

	int update(@Param("account") Account account);

	int delete(@Param("account") Account account);
}
