package org.bitbucket.honourednihilist.moneyrest.rest;

import io.javalin.Context;

public interface TransactionController {

	void createDeposit(Context ctx);

	void createWithdrawal(Context ctx);

	void createTransfer(Context ctx);
}
