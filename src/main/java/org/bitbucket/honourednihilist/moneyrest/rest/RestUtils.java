package org.bitbucket.honourednihilist.moneyrest.rest;

import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.ConcurrentModificationServiceException;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.IllegalArgumentServiceException;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.NotFoundServiceException;

import java.util.UUID;
import java.util.function.Supplier;

import io.javalin.BadRequestResponse;
import io.javalin.ConflictResponse;
import io.javalin.Context;
import io.javalin.NotFoundResponse;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RestUtils {

	public static UUID parseUuid(String uuidString) {
		try {
			return UUID.fromString(uuidString);
		} catch (NullPointerException | IllegalArgumentException e) {
			throw new BadRequestResponse();
		}
	}

	public static <T> T bodyAsClass(Context ctx, Class<T> clazz) {
		try {
			return ctx.bodyAsClass(clazz);
		} catch (Exception e) {
			throw new BadRequestResponse();
		}
	}

	public static void tryExecute(Runnable runnable) {
		tryExecute(() -> {
			runnable.run();
			return true;
		});
	}

	public static <T> T tryExecute(Supplier<T> supplier) {
		try {
			return supplier.get();
		} catch (IllegalArgumentServiceException e) {
			throw new BadRequestResponse(e.getMessage());
		} catch (NotFoundServiceException e) {
			throw new NotFoundResponse(e.getMessage());
		} catch (ConcurrentModificationServiceException e) {
			throw new ConflictResponse(e.getMessage());
		}
	}
}
