package org.bitbucket.honourednihilist.moneyrest.rest.model;

import java.util.Currency;

import lombok.Data;

@Data
public class AccountCreationDto {

	private Currency currency;
}
