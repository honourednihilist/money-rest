package org.bitbucket.honourednihilist.moneyrest.rest;

import org.bitbucket.honourednihilist.moneyrest.rest.model.DepositDto;
import org.bitbucket.honourednihilist.moneyrest.rest.model.TransferDto;
import org.bitbucket.honourednihilist.moneyrest.rest.model.WithdrawalDto;
import org.bitbucket.honourednihilist.moneyrest.service.TransactionService;

import io.javalin.Context;

import static org.bitbucket.honourednihilist.moneyrest.rest.RestUtils.bodyAsClass;
import static org.bitbucket.honourednihilist.moneyrest.rest.RestUtils.tryExecute;
import static org.eclipse.jetty.http.HttpStatus.CREATED_201;

public class TransactionControllerImpl implements TransactionController {

	private final TransactionService transactions;

	public TransactionControllerImpl(TransactionService transactions) {
		this.transactions = transactions;
	}

	@Override
	public void createDeposit(Context ctx) {
		var request = bodyAsClass(ctx, DepositDto.class);
		var ret = tryExecute(() -> transactions.createDeposit(request.getTarget(), request.getAmount(), request.getCurrency()));
		ctx.json(ret).status(CREATED_201);
	}

	@Override
	public void createWithdrawal(Context ctx) {
		var request = bodyAsClass(ctx, WithdrawalDto.class);
		var ret = tryExecute(() -> transactions.createWithdrawal(request.getSource(), request.getAmount(), request.getCurrency()));
		ctx.json(ret).status(CREATED_201);
	}

	@Override
	public void createTransfer(Context ctx) {
		var request = bodyAsClass(ctx, TransferDto.class);
		var ret = tryExecute(() -> transactions.createTransfer(request.getSource(), request.getTarget(), request.getAmount(), request.getCurrency()));
		ctx.json(ret).status(CREATED_201);
	}
}
