package org.bitbucket.honourednihilist.moneyrest.rest.model;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

import lombok.Data;

@Data
public class TransferDto {

	private UUID source;
	private UUID target;
	private BigDecimal amount;
	private Currency currency;
}
