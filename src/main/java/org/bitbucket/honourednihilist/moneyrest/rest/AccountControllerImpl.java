package org.bitbucket.honourednihilist.moneyrest.rest;

import org.bitbucket.honourednihilist.moneyrest.rest.model.AccountCollectionDto;
import org.bitbucket.honourednihilist.moneyrest.rest.model.AccountCreationDto;
import org.bitbucket.honourednihilist.moneyrest.rest.model.AccountDto;
import org.bitbucket.honourednihilist.moneyrest.service.AccountService;

import io.javalin.Context;
import io.javalin.NotFoundResponse;

import static org.bitbucket.honourednihilist.moneyrest.rest.RestUtils.bodyAsClass;
import static org.bitbucket.honourednihilist.moneyrest.rest.RestUtils.parseUuid;
import static org.bitbucket.honourednihilist.moneyrest.rest.RestUtils.tryExecute;
import static org.eclipse.jetty.http.HttpStatus.CREATED_201;
import static org.eclipse.jetty.http.HttpStatus.NO_CONTENT_204;

public class AccountControllerImpl implements AccountController {

	private final AccountService accounts;

	public AccountControllerImpl(AccountService accounts) {
		this.accounts = accounts;
	}

	@Override
	public void getAll(Context ctx) {
		ctx.json(new AccountCollectionDto(accounts.getAll()));
	}

	@Override
	public void getOne(Context ctx, String resourceId) {
		var account = tryExecute(() -> accounts.get(parseUuid(resourceId)));

		if (account == null) {
			throw new NotFoundResponse("Account [" + resourceId + "] does not exist");
		}

		ctx.json(new AccountDto(account));
	}

	@Override
	public void create(Context ctx) {
		var currency = bodyAsClass(ctx, AccountCreationDto.class).getCurrency();
		var account = tryExecute(() -> accounts.create(currency));
		ctx.json(new AccountDto(account)).status(CREATED_201);
	}

	@Override
	public void delete(Context ctx, String resourceId) {
		tryExecute(() -> accounts.delete(parseUuid(resourceId)));
		ctx.status(NO_CONTENT_204);
	}
}
