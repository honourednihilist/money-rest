package org.bitbucket.honourednihilist.moneyrest.rest.model;

import org.bitbucket.honourednihilist.moneyrest.model.Account;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.UUID;

public class AccountDto {

	private final Account account;

	public AccountDto(Account account) {
		this.account = account;
	}

	public UUID getId() {
		return account.getId();
	}

	public BigDecimal getBalance() {
		return account.getBalance();
	}

	public Currency getCurrency() {
		return account.getCurrency();
	}

	public Instant getCreated() {
		return account.getCreated();
	}

	public Instant getUpdated() {
		return account.getUpdated();
	}
}
