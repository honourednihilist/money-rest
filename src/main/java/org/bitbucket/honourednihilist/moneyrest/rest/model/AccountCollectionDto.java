package org.bitbucket.honourednihilist.moneyrest.rest.model;

import org.bitbucket.honourednihilist.moneyrest.model.Account;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class AccountCollectionDto {

	private final List<AccountDto> accounts;

	public AccountCollectionDto(List<Account> accounts) {
		this.accounts = accounts.stream()
				.map(AccountDto::new)
				.collect(toList());
	}

	public List<AccountDto> getAccounts() {
		return accounts;
	}
}
