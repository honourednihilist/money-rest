package org.bitbucket.honourednihilist.moneyrest.rest;

import io.javalin.Context;

public interface AccountController {

	void getAll(Context ctx);

	void getOne(Context ctx, String resourceId);

	void create(Context ctx);

	void delete(Context ctx, String resourceId);
}
