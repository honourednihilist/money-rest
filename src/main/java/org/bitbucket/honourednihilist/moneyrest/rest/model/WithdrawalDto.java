package org.bitbucket.honourednihilist.moneyrest.rest.model;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

import lombok.Data;

@Data
public class WithdrawalDto {

	private UUID source;
	private BigDecimal amount;
	private Currency currency;
}
