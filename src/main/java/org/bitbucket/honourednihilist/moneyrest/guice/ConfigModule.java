package org.bitbucket.honourednihilist.moneyrest.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class ConfigModule extends AbstractModule {

	@Provides
	@Singleton
	public Config createConfig() {
		return ConfigFactory.load();
	}
}
