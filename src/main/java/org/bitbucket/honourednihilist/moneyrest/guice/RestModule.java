package org.bitbucket.honourednihilist.moneyrest.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.typesafe.config.Config;

import org.bitbucket.honourednihilist.moneyrest.rest.AccountController;
import org.bitbucket.honourednihilist.moneyrest.rest.AccountControllerImpl;
import org.bitbucket.honourednihilist.moneyrest.rest.TransactionController;
import org.bitbucket.honourednihilist.moneyrest.rest.TransactionControllerImpl;
import org.bitbucket.honourednihilist.moneyrest.service.AccountService;
import org.bitbucket.honourednihilist.moneyrest.service.TransactionService;
import org.eclipse.jetty.http.MimeTypes;

import io.javalin.Javalin;
import io.javalin.json.JavalinJackson;

public class RestModule extends AbstractModule {

	@Provides
	@Singleton
	public AccountController createAccountController(AccountService accountService) {
		return new AccountControllerImpl(accountService);
	}

	@Provides
	@Singleton
	public TransactionController createTransactionController(TransactionService transactionService) {
		return new TransactionControllerImpl(transactionService);
	}

	@Provides
	@Singleton
	public Javalin createRest(Config config, AccountController accountController, TransactionController transactionController) {
		JavalinJackson.getObjectMapper()
				.registerModule(new JavaTimeModule())
				.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
				.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

		var rest = Javalin.create()
				.port(config.getInt("money-rest.rest.port"))
				.disableStartupBanner()
				.defaultContentType(MimeTypes.Type.APPLICATION_JSON.asString())
				.get("/rest/1.0/hello", ctx -> ctx.result("Hello Revolut!"))

				.get("/rest/1.0/accounts", accountController::getAll)
				.get("/rest/1.0/accounts/:id", ctx -> accountController.getOne(ctx, ctx.pathParam("id")))
				.post("/rest/1.0/accounts", accountController::create)
				.delete("/rest/1.0/accounts/:id", ctx -> accountController.delete(ctx, ctx.pathParam("id")))

				.post("/rest/1.0/transactions/deposit", transactionController::createDeposit)
				.post("/rest/1.0/transactions/withdrawal", transactionController::createWithdrawal)
				.post("/rest/1.0/transactions/transfer", transactionController::createTransfer);

		Runtime.getRuntime().addShutdownHook(new Thread(rest::stop, "rest-shutdown-hook"));
		return rest;
	}
}
