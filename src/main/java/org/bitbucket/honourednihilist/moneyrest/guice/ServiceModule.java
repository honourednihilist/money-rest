package org.bitbucket.honourednihilist.moneyrest.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import org.apache.ibatis.session.SqlSessionManager;
import org.bitbucket.honourednihilist.moneyrest.service.AccountService;
import org.bitbucket.honourednihilist.moneyrest.service.AccountServiceImpl;
import org.bitbucket.honourednihilist.moneyrest.service.TransactionService;
import org.bitbucket.honourednihilist.moneyrest.service.TransactionServiceImpl;

public class ServiceModule extends AbstractModule {

	@Provides
	@Singleton
	public AccountService createAccountService(SqlSessionManager sqlSessionManager) {
		return new AccountServiceImpl(sqlSessionManager);
	}

	@Provides
	@Singleton
	public TransactionService createTransactionService(SqlSessionManager sqlSessionManager) {
		return new TransactionServiceImpl(sqlSessionManager);
	}
}
