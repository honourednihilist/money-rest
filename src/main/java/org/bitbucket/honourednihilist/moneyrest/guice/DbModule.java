package org.bitbucket.honourednihilist.moneyrest.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.SqlSessionManager;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.bitbucket.honourednihilist.moneyrest.dao.AccountMapper;
import org.bitbucket.honourednihilist.moneyrest.dao.CurrencyTypeHandler;
import org.bitbucket.honourednihilist.moneyrest.dao.UuidTypeHandler;

import javax.sql.DataSource;

public class DbModule extends AbstractModule {

	@Provides
	@Singleton
	public DataSource createDataSource(Config config) {
		var configuration = new HikariConfig();
		configuration.setDriverClassName(config.getString("money-rest.db.driver"));
		configuration.setJdbcUrl(config.getString("money-rest.db.url"));
		configuration.setUsername(config.getString("money-rest.db.username"));
		configuration.setPassword(config.getString("money-rest.db.password"));
		configuration.setTransactionIsolation("TRANSACTION_READ_COMMITTED");
		configuration.setMinimumIdle(1);

		var dataSource =  new HikariDataSource(configuration);
		Runtime.getRuntime().addShutdownHook(new Thread(dataSource::close));

		return dataSource;
	}

	@Provides
	@Singleton
	public SqlSessionFactory createSqlSessionFactory(DataSource dataSource) {
		var configuration = new Configuration();
		configuration.setEnvironment(new Environment(this.getClass().getSimpleName(), new JdbcTransactionFactory(), dataSource));
		configuration.getTypeHandlerRegistry().register(UuidTypeHandler.class);
		configuration.getTypeHandlerRegistry().register(CurrencyTypeHandler.class);
		new XMLMapperBuilder(this.getClass().getResourceAsStream("/db/account-mapper.xml"),
				configuration, AccountMapper.class.getSimpleName(), configuration.getSqlFragments()).parse();
		return new SqlSessionFactoryBuilder().build(configuration);
	}

	@Provides
	@Singleton
	public SqlSessionManager createSqlSessionManager(SqlSessionFactory sqlSessionFactory) {
		return SqlSessionManager.newInstance(sqlSessionFactory);
	}
}
