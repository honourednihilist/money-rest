package org.bitbucket.honourednihilist.moneyrest.service;

import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.IllegalArgumentServiceException;

import java.util.Currency;
import java.util.UUID;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ServiceUtils {

	public static UUID requireNonNullId(UUID id) {
		return requireNonNull(id, "id is required");
	}

	public static Currency requireNonNullCurrency(Currency currency) {
		return requireNonNull(currency, "currency is required");
	}

	public static <T> T requireNonNull(T obj, String message) {
		if (obj == null) {
			throw new IllegalArgumentServiceException(message);
		}

		return obj;
	}
}
