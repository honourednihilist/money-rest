package org.bitbucket.honourednihilist.moneyrest.service;

import org.apache.ibatis.session.SqlSessionManager;
import org.bitbucket.honourednihilist.moneyrest.dao.AccountMapper;
import org.bitbucket.honourednihilist.moneyrest.model.Account;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.ConcurrentModificationServiceException;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.IllegalArgumentServiceException;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.NotFoundServiceException;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.List;
import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.bitbucket.honourednihilist.moneyrest.service.ServiceUtils.requireNonNullCurrency;
import static org.bitbucket.honourednihilist.moneyrest.service.ServiceUtils.requireNonNullId;

public class AccountServiceImpl implements AccountService {

	private final SqlSessionManager sessionManager;

	public AccountServiceImpl(SqlSessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	@Override
	public List<Account> getAll() {
		return sessionManager.getMapper(AccountMapper.class).getAll();
	}

	@Override
	public Account get(UUID id) {
		requireNonNullId(id);
		return sessionManager.getMapper(AccountMapper.class).get(id);
	}

	@Override
	public Account create(Currency currency) {
		requireNonNullCurrency(currency);

		var account = new Account();
		account.setId(randomUUID());
		account.setBalance(BigDecimal.ZERO);
		account.setCurrency(currency);
		account.setCreated(Instant.now());
		account.setUpdated(account.getCreated());
		account.setVersion(1);

		sessionManager.getMapper(AccountMapper.class).create(account);
		return account;
	}

	@Override
	public void delete(UUID id) {
		requireNonNullId(id);

		try (var session = sessionManager.openSession()) {
			var accountMapper = session.getMapper(AccountMapper.class);
			var account = accountMapper.get(id);

			if (account == null) {
				throw new NotFoundServiceException("Account [" + id + "] does not exist");
			}

			if (account.getBalance().compareTo(BigDecimal.ZERO) != 0) {
				throw new IllegalArgumentServiceException("Account [" + id + "] has non-zero balance");
			}

			account.setUpdated(Instant.now());

			if (accountMapper.delete(account) == 0) {
				// Either somebody has deleted this account already or the account was modified by a transaction (e.g. deposit)
				throw new ConcurrentModificationServiceException("Account [" + id + "] has been changed concurrently");
			}

			session.commit();
		}
	}
}
