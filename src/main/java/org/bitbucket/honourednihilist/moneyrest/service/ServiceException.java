package org.bitbucket.honourednihilist.moneyrest.service;

public class ServiceException extends RuntimeException {

	public ServiceException(String message) {
		super(message);
	}

	public static class IllegalArgumentServiceException extends ServiceException {

		public IllegalArgumentServiceException(String message) {
			super(message);
		}
	}

	public static class NotFoundServiceException extends ServiceException {

		public NotFoundServiceException(String message) {
			super(message);
		}
	}

	public static class ConcurrentModificationServiceException extends ServiceException {

		public ConcurrentModificationServiceException(String message) {
			super(message);
		}
	}
}
