package org.bitbucket.honourednihilist.moneyrest.service;

import org.bitbucket.honourednihilist.moneyrest.model.Account;

import java.util.Currency;
import java.util.List;
import java.util.UUID;

public interface AccountService {

	List<Account> getAll();

	Account get(UUID id);

	Account create(Currency currency);

	void delete(UUID id);
}
