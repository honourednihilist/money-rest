package org.bitbucket.honourednihilist.moneyrest.service;

import org.apache.ibatis.session.SqlSessionManager;
import org.bitbucket.honourednihilist.moneyrest.dao.AccountMapper;
import org.bitbucket.honourednihilist.moneyrest.model.Transaction;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.IllegalArgumentServiceException;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.bitbucket.honourednihilist.moneyrest.service.ServiceUtils.requireNonNull;
import static org.bitbucket.honourednihilist.moneyrest.service.ServiceUtils.requireNonNullCurrency;

public class TransactionServiceImpl implements TransactionService {

	private final SqlSessionManager sessionManager;

	public TransactionServiceImpl(SqlSessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	@Override
	public Transaction createDeposit(UUID target, BigDecimal amount, Currency currency) {
		requireNonNull(target, "target id is required");
		requireNonNullCurrency(currency);
		verifyAmount(amount, currency);

		try (var session = sessionManager.openSession()) {
			var accountMapper = session.getMapper(AccountMapper.class);
			var account = accountMapper.get(target);

			if (account == null) {
				return accountNotFound(target);
			}

			if (!account.getCurrency().equals(currency)) {
				return accountNotInCurrency(target, currency);
			}

			account.setBalance(account.getBalance().add(amount));
			account.setUpdated(Instant.now());

			if (accountMapper.update(account) == 0) {
				return failed(target);
			}

			session.commit();
		}

		return completed();
	}

	@Override
	public Transaction createWithdrawal(UUID source, BigDecimal amount, Currency currency) {
		requireNonNull(source, "source id is required");
		requireNonNullCurrency(currency);
		verifyAmount(amount, currency);

		try (var session = sessionManager.openSession()) {
			var accountMapper = session.getMapper(AccountMapper.class);
			var account = accountMapper.get(source);

			if (account == null) {
				return accountNotFound(source);
			}

			if (!account.getCurrency().equals(currency)) {
				return accountNotInCurrency(source, currency);
			}

			account.setBalance(account.getBalance().subtract(amount));
			account.setUpdated(Instant.now());

			if (account.getBalance().compareTo(BigDecimal.ZERO) < 0) {
				return notEnoughMoney(source);
			}

			if (accountMapper.update(account) == 0) {
				return failed(source);
			}

			session.commit();
		}

		return completed();
	}

	@Override
	public Transaction createTransfer(UUID source, UUID target, BigDecimal amount, Currency currency) {
		requireNonNull(source, "source id is required");
		requireNonNull(target, "target id is required");
		requireNonNullCurrency(currency);
		verifyAmount(amount, currency);

		try (var session = sessionManager.openSession()) {
			var accountMapper = session.getMapper(AccountMapper.class);
			var from = accountMapper.get(source);

			if (from == null) {
				return accountNotFound(source);
			}

			var to = accountMapper.get(target);

			if (to == null) {
				return accountNotFound(target);
			}

			if (!from.getCurrency().equals(currency)) {
				return accountNotInCurrency(source, currency);
			}

			if (!to.getCurrency().equals(currency)) {
				return accountsNotInTheSameCurrency(source, target, currency);
			}

			if (source.equals(target)) {
				return notDifferentAccounts(source);
			}

			from.setBalance(from.getBalance().subtract(amount));
			from.setUpdated(Instant.now());

			to.setBalance(to.getBalance().add(amount));
			to.setUpdated(from.getUpdated());

			if (from.getBalance().compareTo(BigDecimal.ZERO) < 0) {
				return notEnoughMoney(source);
			}

			if (accountMapper.update(from) == 0) {
				return failed(source);
			}

			if (accountMapper.update(to) == 0) {
				return failed(target);
			}

			session.commit();
		}

		return completed();
	}

	private static void verifyAmount(BigDecimal amount, Currency currency) {
		requireNonNull(amount, "amount is required");

		if (amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentServiceException("amount must be positive");
		}

		var fractionalPart = amount.remainder(BigDecimal.ONE).stripTrailingZeros();

		if (fractionalPart.compareTo(BigDecimal.ZERO) != 0 && currency.getDefaultFractionDigits() < fractionalPart.scale()) {
			throw new IllegalArgumentServiceException("Number of fraction digits used with currency [" + currency.getCurrencyCode() + "] " +
					"must be not greater than [" + currency.getDefaultFractionDigits() + "]");
		}
	}

	private static Transaction completed() {
		return new Transaction(randomUUID(), Transaction.Status.COMPLETED, Instant.now(), null);
	}

	private static Transaction accountNotFound(UUID id) {
		return new Transaction(randomUUID(), Transaction.Status.DECLINED, Instant.now(),
				"Account [" + id + "] does not exist");
	}

	private static Transaction accountNotInCurrency(UUID id, Currency currency) {
		return new Transaction(randomUUID(), Transaction.Status.DECLINED, Instant.now(),
				"Account [" + id + "] is in a different currency than [" + currency.getCurrencyCode() + "]");
	}

	private static Transaction accountsNotInTheSameCurrency(UUID source, UUID target, Currency currency) {
		return new Transaction(randomUUID(), Transaction.Status.DECLINED, Instant.now(),
				"Both source account [" + source + "] and target account [" + target + "] " +
						"must be in the same currency [" + currency.getCurrencyCode() + "]");
	}

	private static Transaction notEnoughMoney(UUID id) {
		return new Transaction(randomUUID(), Transaction.Status.DECLINED, Instant.now(),
				"Account [" + id + "] has insufficient balance for the requested operation");
	}

	private Transaction notDifferentAccounts(UUID id) {
		return new Transaction(randomUUID(), Transaction.Status.DECLINED, Instant.now(),
				"Both source account and target account are actually the same account [" + id + "]");
	}

	private static Transaction failed(UUID id) {
		return new Transaction(randomUUID(), Transaction.Status.FAILED, Instant.now(),
				"Account [" + id + "] has been changed concurrently");
	}
}
