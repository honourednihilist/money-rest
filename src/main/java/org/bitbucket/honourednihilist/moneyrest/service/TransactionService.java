package org.bitbucket.honourednihilist.moneyrest.service;

import org.bitbucket.honourednihilist.moneyrest.model.Transaction;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

public interface TransactionService {

	Transaction createDeposit(UUID target, BigDecimal amount, Currency currency);

	Transaction createWithdrawal(UUID source, BigDecimal amount, Currency currency);

	Transaction createTransfer(UUID source, UUID target, BigDecimal amount, Currency currency);
}
