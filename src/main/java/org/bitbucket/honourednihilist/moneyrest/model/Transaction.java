package org.bitbucket.honourednihilist.moneyrest.model;

import java.time.Instant;
import java.util.UUID;

import lombok.Data;

@Data
public class Transaction {

	private final UUID id;
	private final Status status;
	private final Instant created;
	private final String message;

	public enum Status {
		COMPLETED, DECLINED, FAILED
	}
}
