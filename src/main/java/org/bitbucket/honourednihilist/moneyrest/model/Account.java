package org.bitbucket.honourednihilist.moneyrest.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.UUID;

import lombok.Data;

@Data
public class Account {

	private UUID id;
	private BigDecimal balance;
	private Currency currency;
	private Instant created;
	private Instant updated;
	private long version;
}
