CREATE TABLE IF NOT EXISTS accounts (
    id VARCHAR(36) PRIMARY KEY,
    balance DECIMAL(19, 4) NOT NULL,
    currency VARCHAR(3) NOT NULL,
    created TIMESTAMP NOT NULL,
    updated TIMESTAMP NOT NULL,
    version BIGINT NOT NULL,
    deleted BOOLEAN NOT NULL
);

CREATE INDEX IF NOT EXISTS accounts_deleted ON accounts(deleted);
