
package org.bitbucket.honourednihilist.moneyrest.rest;

import org.bitbucket.honourednihilist.moneyrest.rest.ApiTestUtils.TransactionTestDto;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static java.lang.Thread.sleep;
import static java.util.UUID.randomUUID;
import static org.bitbucket.honourednihilist.moneyrest.rest.ApiTestUtils.DEPOSIT_URL;
import static org.bitbucket.honourednihilist.moneyrest.rest.ApiTestUtils.TRANSFER_URL;
import static org.bitbucket.honourednihilist.moneyrest.rest.ApiTestUtils.WITHDRAWAL_URL;
import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;
import static org.eclipse.jetty.http.HttpStatus.CREATED_201;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

class TransactionControllerImplTest extends BaseRestControllerTest {

	@Test
	void testSimpleMoneyFlow() throws Exception {
		var accountFrom = ApiTestUtils.createAccount("RUB");
		var accountTo = ApiTestUtils.createAccount("RUB");

		var beforeDeposit = Instant.now();
		sleep(100);

		var deposit = ApiTestUtils.createDeposit(accountFrom.getId(), 100d, "RUB");
		assertThat(deposit.getId(), is(notNullValue()));
		assertThat(deposit.getStatus(), is("COMPLETED"));
		assertThat(deposit.getCreated().isAfter(beforeDeposit), is(true));
		assertThat(deposit.getMessage(), is(nullValue()));

		var accountFromAfterDeposit = ApiTestUtils.getAccount(accountFrom.getId());
		assertThat(accountFromAfterDeposit.getBalance(), is(100d));
		assertThat(accountFromAfterDeposit.getCreated(), is(accountFrom.getCreated()));
		assertThat(accountFromAfterDeposit.getUpdated().isAfter(accountFrom.getUpdated()), is(true));

		var accountToAfterDeposit = ApiTestUtils.getAccount(accountTo.getId());
		assertThat(accountToAfterDeposit, is(accountTo));

		var beforeTransfer = Instant.now();
		sleep(100);

		var transfer = ApiTestUtils.createTransfer(accountFrom.getId(), accountTo.getId(), 100d, "RUB");
		assertThat(transfer.getId(), is(notNullValue()));
		assertThat(transfer.getStatus(), is("COMPLETED"));
		assertThat(transfer.getCreated().isAfter(beforeTransfer), is(true));
		assertThat(transfer.getMessage(), is(nullValue()));

		var accountFromAfterTransfer = ApiTestUtils.getAccount(accountFrom.getId());
		assertThat(accountFromAfterTransfer.getBalance(), is(0d));
		assertThat(accountFromAfterTransfer.getCreated(), is(accountFrom.getCreated()));
		assertThat(accountFromAfterTransfer.getUpdated().isAfter(accountFromAfterDeposit.getUpdated()), is(true));

		var accountToAfterTransfer = ApiTestUtils.getAccount(accountTo.getId());
		assertThat(accountToAfterTransfer.getBalance(), is(100d));
		assertThat(accountToAfterTransfer.getCreated(), is(accountTo.getCreated()));
		assertThat(accountToAfterTransfer.getUpdated().isAfter(accountToAfterDeposit.getUpdated()), is(true));
		assertThat(accountToAfterTransfer.getUpdated(), is(accountFromAfterTransfer.getUpdated()));

		var beforeWithdrawal = Instant.now();
		sleep(100);

		var withdrawal = ApiTestUtils.createWithdrawal(accountTo.getId(), 100d, "RUB");
		assertThat(withdrawal.getId(), is(notNullValue()));
		assertThat(withdrawal.getStatus(), is("COMPLETED"));
		assertThat(withdrawal.getCreated().isAfter(beforeWithdrawal), is(true));
		assertThat(withdrawal.getMessage(), is(nullValue()));

		var accountFromAfterWithdrawal = ApiTestUtils.getAccount(accountFrom.getId());
		assertThat(accountFromAfterWithdrawal, is(accountFromAfterTransfer));

		var accountToAfterWithdrawal = ApiTestUtils.getAccount(accountTo.getId());
		assertThat(accountToAfterWithdrawal.getBalance(), is(0d));
		assertThat(accountToAfterWithdrawal.getCreated(), is(accountTo.getCreated()));
		assertThat(accountToAfterWithdrawal.getUpdated().isAfter(accountToAfterTransfer.getUpdated()), is(true));

		ApiTestUtils.deleteAccount(accountFrom.getId());
		ApiTestUtils.deleteAccount(accountTo.getId());
	}

	@Test
	void testCreateTransactionCommonErrors() {
		for (String url : List.of(DEPOSIT_URL, WITHDRAWAL_URL, TRANSFER_URL)) {
			when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body("")
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body("\n\n\n\n")
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body("{")
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body("{}")
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body(Map.of(
					"source", randomUUID(),
					"target", randomUUID(),
					"noAmount", 1.23,
					"currency", "USD"))
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body(Map.of(
					"source", randomUUID(),
					"target", randomUUID(),
					"amount", "three hundred",
					"currency", "USD"))
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body(Map.of(
					"source", randomUUID(),
					"target", randomUUID(),
					"amount", 0,
					"currency", "USD"))
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body(Map.of(
					"source", randomUUID(),
					"target", randomUUID(),
					"amount", -0.03,
					"currency", "USD"))
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body(Map.of(
					"source", randomUUID(),
					"target", randomUUID(),
					"amount", -0.03,
					"noCurrency", "USD"))
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body(Map.of(
					"source", randomUUID(),
					"target", randomUUID(),
					"amount", 1.23,
					"currency", 42))
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body(Map.of(
					"source", randomUUID(),
					"target", randomUUID(),
					"amount", 1.23,
					"currency", "USSR"))
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);

			given().body(Map.of(
					"source", randomUUID(),
					"target", randomUUID(),
					"amount", 1.234,
					"currency", "USD"))
					.when().post(url)
					.then().statusCode(BAD_REQUEST_400);
		}
	}

	@Test
	void testCreateDepositErrors() throws Exception {
		given().body(Map.of("noTarget", randomUUID(), "amount", 1.23, "currency", "USD"))
				.when().post(DEPOSIT_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of("target", 42, "amount", 1.23, "currency", "USD"))
				.when().post(DEPOSIT_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of("target", "invalid-uuid", "amount", 1.23, "currency", "USD"))
				.when().post(DEPOSIT_URL)
				.then().statusCode(BAD_REQUEST_400);

		var before = Instant.now();
		sleep(100);

		var notFound = ApiTestUtils.createDeposit(randomUUID(), 1.23, "USD");
		assertThat(notFound.getStatus(), is("DECLINED"));
		assertThat(notFound.getCreated().isAfter(before), is(true));
		assertThat(notFound.getMessage(), containsString("does not exist"));

		before = Instant.now();
		sleep(100);

		var account = ApiTestUtils.createAccount("RUB");
		var notInCurrency = ApiTestUtils.createDeposit(account.getId(), 1.23, "USD");
		assertThat(notInCurrency.getStatus(), is("DECLINED"));
		assertThat(notInCurrency.getCreated().isAfter(before), is(true));
		assertThat(notInCurrency.getMessage(), containsString("is in a different currency"));

		before = Instant.now();
		sleep(100);

		var deposit = given().body(Map.of(
				"target", account.getId(),
				"amount", 1.23,
				"currency", account.getCurrency(),
				"some", "field"))
				.when().post(DEPOSIT_URL)
				.then().statusCode(CREATED_201)
				.extract().body().as(TransactionTestDto.class);
		assertThat(deposit.getStatus(), is("COMPLETED"));
		assertThat(deposit.getCreated().isAfter(before), is(true));
		assertThat(deposit.getMessage(), is(nullValue()));

		ApiTestUtils.withdrawAllAndDeleteAccount(account.getId());
	}

	@Test
	void testCreateWithdrawalErrors() throws Exception {
		given().body(Map.of("noSource", randomUUID(), "amount", 1.23, "currency", "USD"))
				.when().post(WITHDRAWAL_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of("source", 42, "amount", 1.23, "currency", "USD"))
				.when().post(WITHDRAWAL_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of("source", "invalid-uuid", "amount", 1.23, "currency", "USD"))
				.when().post(WITHDRAWAL_URL)
				.then().statusCode(BAD_REQUEST_400);

		var before = Instant.now();
		sleep(100);

		var notFound = ApiTestUtils.createWithdrawal(randomUUID(), 1.23, "USD");
		assertThat(notFound.getStatus(), is("DECLINED"));
		assertThat(notFound.getCreated().isAfter(before), is(true));
		assertThat(notFound.getMessage(), containsString("does not exist"));

		before = Instant.now();
		sleep(100);

		var account = ApiTestUtils.createAccount("RUB");
		var notInCurrency = ApiTestUtils.createWithdrawal(account.getId(), 1.23, "USD");
		assertThat(notInCurrency.getStatus(), is("DECLINED"));
		assertThat(notInCurrency.getCreated().isAfter(before), is(true));
		assertThat(notInCurrency.getMessage(), containsString("is in a different currency"));

		before = Instant.now();
		sleep(100);

		var notEnoughMoney = ApiTestUtils.createWithdrawal(account.getId(), 1.23, "RUB");
		assertThat(notEnoughMoney.getStatus(), is("DECLINED"));
		assertThat(notEnoughMoney.getCreated().isAfter(before), is(true));
		assertThat(notEnoughMoney.getMessage(), containsString("insufficient balance"));

		before = Instant.now();
		sleep(100);

		ApiTestUtils.createDeposit(account.getId(), 1.23, "RUB");
		var withdrawal = given().body(Map.of(
				"source", account.getId(),
				"amount", 1.23,
				"currency", account.getCurrency(),
				"some", "field"))
				.when().post(WITHDRAWAL_URL)
				.then().statusCode(CREATED_201)
				.extract().body().as(TransactionTestDto.class);
		assertThat(withdrawal.getStatus(), is("COMPLETED"));
		assertThat(withdrawal.getCreated().isAfter(before), is(true));
		assertThat(withdrawal.getMessage(), is(nullValue()));

		ApiTestUtils.deleteAccount(account.getId());
	}

	@Test
	void testCreateTransfer() throws Exception {
		given().body(Map.of(
				"noSource", randomUUID(),
				"target", randomUUID(),
				"amount", 1.23,
				"currency", "USD"))
				.when().post(TRANSFER_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of(
				"source", 73,
				"target", randomUUID(),
				"amount", 1.23,
				"currency", "USD"))
				.when().post(TRANSFER_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of(
				"source", "invalid-uuid",
				"target", randomUUID(),
				"amount", 1.23,
				"currency", "USD"))
				.when().post(TRANSFER_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of(
				"source", randomUUID(),
				"noTarget", randomUUID(),
				"amount", 1.23,
				"currency", "USD"))
				.when().post(TRANSFER_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of(
				"source", randomUUID(),
				"target", 73,
				"amount", 1.23,
				"currency", "USD"))
				.when().post(TRANSFER_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of(
				"source", randomUUID(),
				"target", "invalid-uuid",
				"amount", 1.23,
				"currency", "USD"))
				.when().post(TRANSFER_URL)
				.then().statusCode(BAD_REQUEST_400);

		var source = ApiTestUtils.createAccount("RUB");
		var targetUsd = ApiTestUtils.createAccount("USD");
		var before = Instant.now();
		sleep(100);

		var notFoundSource = ApiTestUtils.createTransfer(randomUUID(), targetUsd.getId(), 1.23, "USD");
		assertThat(notFoundSource.getStatus(), is("DECLINED"));
		assertThat(notFoundSource.getCreated().isAfter(before), is(true));
		assertThat(notFoundSource.getMessage(), containsString("does not exist"));

		before = Instant.now();
		sleep(100);

		var notFoundTarget = ApiTestUtils.createTransfer(source.getId(), randomUUID(), 1.23, "USD");
		assertThat(notFoundTarget.getStatus(), is("DECLINED"));
		assertThat(notFoundTarget.getCreated().isAfter(before), is(true));
		assertThat(notFoundTarget.getMessage(), containsString("does not exist"));

		before = Instant.now();
		sleep(100);

		var sourceNotInCurrency = ApiTestUtils.createTransfer(source.getId(), targetUsd.getId(), 1.23, "USD");
		assertThat(sourceNotInCurrency.getStatus(), is("DECLINED"));
		assertThat(sourceNotInCurrency.getCreated().isAfter(before), is(true));
		assertThat(sourceNotInCurrency.getMessage(), containsString("is in a different currency"));

		before = Instant.now();
		sleep(100);

		var bothNotInCurrency = ApiTestUtils.createTransfer(source.getId(), targetUsd.getId(), 1.23, "RUB");
		assertThat(bothNotInCurrency.getStatus(), is("DECLINED"));
		assertThat(bothNotInCurrency.getCreated().isAfter(before), is(true));
		assertThat(bothNotInCurrency.getMessage(), containsString("must be in the same currency"));

		before = Instant.now();
		sleep(100);

		var notDifferentAccounts = ApiTestUtils.createTransfer(source.getId(), source.getId(), 1.23, "RUB");
		assertThat(notDifferentAccounts.getStatus(), is("DECLINED"));
		assertThat(notDifferentAccounts.getCreated().isAfter(before), is(true));
		assertThat(notDifferentAccounts.getMessage(), containsString("the same account"));

		before = Instant.now();
		sleep(100);

		var targetRub = ApiTestUtils.createAccount("RUB");
		var notEnoughMoney = ApiTestUtils.createTransfer(source.getId(), targetRub.getId(), 1.23, "RUB");
		assertThat(notEnoughMoney.getStatus(), is("DECLINED"));
		assertThat(notEnoughMoney.getCreated().isAfter(before), is(true));
		assertThat(notEnoughMoney.getMessage(), containsString("insufficient balance"));

		before = Instant.now();
		sleep(100);

		ApiTestUtils.createDeposit(source.getId(), 1.23, "RUB");
		var transfer = given().body(Map.of(
				"source", source.getId(),
				"target", targetRub.getId(),
				"amount", 1.23,
				"currency", source.getCurrency(),
				"some", "field"))
				.when().post(TRANSFER_URL)
				.then().statusCode(CREATED_201)
				.extract().body().as(TransactionTestDto.class);
		assertThat(transfer.getStatus(), is("COMPLETED"));
		assertThat(transfer.getCreated().isAfter(before), is(true));
		assertThat(transfer.getMessage(), is(nullValue()));

		ApiTestUtils.deleteAccount(source.getId());
		ApiTestUtils.deleteAccount(targetUsd.getId());
		ApiTestUtils.withdrawAllAndDeleteAccount(targetRub.getId());
	}
}
