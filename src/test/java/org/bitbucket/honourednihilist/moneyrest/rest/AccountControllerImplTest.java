package org.bitbucket.honourednihilist.moneyrest.rest;

import org.bitbucket.honourednihilist.moneyrest.rest.ApiTestUtils.AccountTestDto;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static java.lang.Thread.sleep;
import static org.bitbucket.honourednihilist.moneyrest.rest.ApiTestUtils.ACCOUNTS_URL;
import static org.bitbucket.honourednihilist.moneyrest.rest.ApiTestUtils.ACCOUNT_URL;
import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;
import static org.eclipse.jetty.http.HttpStatus.CREATED_201;
import static org.eclipse.jetty.http.HttpStatus.NOT_FOUND_404;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

class AccountControllerImplTest extends BaseRestControllerTest {

	@Test
	void testCrud() throws Exception {
		var accounts = ApiTestUtils.getAccounts();
		assertThat(accounts.getAccounts(), is(emptyCollectionOf(AccountTestDto.class)));

		var beforeAccountCreation = Instant.now();
		sleep(100);

		var account = ApiTestUtils.createAccount("RUB");
		assertThat(account.getId(), is(notNullValue()));
		assertThat(account.getBalance(), is(0d));
		assertThat(account.getCurrency(), is("RUB"));
		assertThat(account.getCreated().isAfter(beforeAccountCreation), is(true));
		assertThat(account.getUpdated(), is(account.getCreated()));

		accounts = ApiTestUtils.getAccounts();
		assertThat(accounts.getAccounts(), is(containsInAnyOrder(account)));
		assertThat(ApiTestUtils.getAccount(account.getId()), is(account));

		ApiTestUtils.deleteAccount(account.getId());
		accounts = ApiTestUtils.getAccounts();
		assertThat(accounts.getAccounts(), is(emptyCollectionOf(AccountTestDto.class)));

		when().get(ACCOUNT_URL, account.getId()).then().statusCode(NOT_FOUND_404);
	}

	@Test
	void testCreateErrors() {
		when().post(ACCOUNTS_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body("")
				.when().post(ACCOUNTS_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body("\n\n\n\n")
				.when().post(ACCOUNTS_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body("{")
				.when().post(ACCOUNTS_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body("{}")
				.when().post(ACCOUNTS_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of("notCurrency", "value"))
				.when().post(ACCOUNTS_URL)
				.then().statusCode(BAD_REQUEST_400);

		given().body(Map.of("currency", "RRUB", "notCurrency", "value"))
				.when().post(ACCOUNTS_URL)
				.then().statusCode(BAD_REQUEST_400);

		var account = given().body(Map.of("currency", "RUB", "notCurrency", "value"))
				.when().post(ACCOUNTS_URL)
				.then().statusCode(CREATED_201)
				.extract().body().as(AccountTestDto.class);
		ApiTestUtils.deleteAccount(account.getId());
	}

	@Test
	void testGetErrors() {
		when().get(ACCOUNT_URL, "    ")
				.then().statusCode(BAD_REQUEST_400);

		when().get(ACCOUNT_URL, "invalid-uuid")
				.then().statusCode(BAD_REQUEST_400);

		when().get(ACCOUNT_URL, UUID.randomUUID())
				.then().statusCode(NOT_FOUND_404);
	}

	@Test
	void testDeleteErrors() {
		when().delete(ACCOUNT_URL, "    ")
				.then().statusCode(BAD_REQUEST_400);

		when().delete(ACCOUNT_URL, "invalid-uuid")
				.then().statusCode(BAD_REQUEST_400);

		when().delete(ACCOUNT_URL, UUID.randomUUID())
				.then().statusCode(NOT_FOUND_404);

		var account = ApiTestUtils.createAccount("GBP");
		ApiTestUtils.createDeposit(account.getId(), 0.01d, "GBP");

		var error = when().delete(ACCOUNT_URL, account.getId())
				.then().statusCode(BAD_REQUEST_400)
				.extract().jsonPath().getString("title");
		assertThat(error, both(containsString(account.getId().toString())).and(containsString("non-zero balance")));

		ApiTestUtils.createWithdrawal(account.getId(), 0.01d, "GBP");
		ApiTestUtils.deleteAccount(account.getId());

		when().delete(ACCOUNT_URL, account.getId())
				.then().statusCode(NOT_FOUND_404);
	}
}
