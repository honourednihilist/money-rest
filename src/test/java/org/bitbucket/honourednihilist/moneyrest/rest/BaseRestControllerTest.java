package org.bitbucket.honourednihilist.moneyrest.rest;

import org.bitbucket.honourednihilist.moneyrest.Start;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import io.javalin.Javalin;
import io.restassured.RestAssured;

abstract class BaseRestControllerTest {

	private static Javalin rest;

	@BeforeAll
	static void beforeAll() {
		rest = Start.createInjector().getInstance(Javalin.class);
		rest.start();

		RestAssured.port = rest.port();
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	}

	@AfterAll
	static void afterAll() {
		rest.stop();
		RestAssured.reset();
	}
}
