package org.bitbucket.honourednihilist.moneyrest.rest;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.eclipse.jetty.http.HttpStatus.CREATED_201;
import static org.eclipse.jetty.http.HttpStatus.NO_CONTENT_204;
import static org.eclipse.jetty.http.HttpStatus.OK_200;
import static org.hamcrest.Matchers.isEmptyOrNullString;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class ApiTestUtils {

	static final String ACCOUNTS_URL = "/rest/1.0/accounts";
	static final String ACCOUNT_URL = "/rest/1.0/accounts/{id}";
	static final String DEPOSIT_URL = "/rest/1.0/transactions/deposit";
	static final String WITHDRAWAL_URL = "/rest/1.0/transactions/withdrawal";
	static final String TRANSFER_URL = "/rest/1.0/transactions/transfer";

	static AccountCollectionTestDto getAccounts() {
		return when().get(ACCOUNTS_URL)
				.then().statusCode(OK_200)
				.extract().body().as(AccountCollectionTestDto.class);
	}

	static AccountTestDto getAccount(UUID id) {
		return when().get(ACCOUNT_URL, id)
				.then().statusCode(OK_200)
				.extract().body().as(AccountTestDto.class);
	}

	static AccountTestDto createAccount(String currency) {
		return given().body(Map.of("currency", currency))
				.when().post(ACCOUNTS_URL)
				.then().statusCode(CREATED_201)
				.extract().body().as(AccountTestDto.class);
	}

	static void deleteAccount(UUID id) {
		when().delete(ACCOUNT_URL, id)
				.then().statusCode(NO_CONTENT_204)
				.body(isEmptyOrNullString());
	}

	static TransactionTestDto createDeposit(UUID targetId, Double amount, String currency) {
		return given().body(Map.of(
				"target", targetId,
				"amount", amount,
				"currency", currency))
				.when().post(DEPOSIT_URL)
				.then().statusCode(CREATED_201)
				.extract().body().as(TransactionTestDto.class);
	}

	static TransactionTestDto createWithdrawal(UUID sourceId, Double amount, String currency) {
		return given().body(Map.of(
				"source", sourceId,
				"amount", amount,
				"currency", currency))
				.when().post(WITHDRAWAL_URL)
				.then().statusCode(CREATED_201)
				.extract().body().as(TransactionTestDto.class);
	}

	static TransactionTestDto createTransfer(UUID sourceId, UUID targetId, Double amount, String currency) {
		return given().body(Map.of(
				"source", sourceId,
				"target", targetId,
				"amount", amount,
				"currency", currency))
				.when().post(TRANSFER_URL)
				.then().statusCode(CREATED_201)
				.extract().body().as(TransactionTestDto.class);
	}

	static void withdrawAllAndDeleteAccount(UUID id) {
		var account = getAccount(id);
		createWithdrawal(account.getId(), account.getBalance(), account.getCurrency());
		deleteAccount(id);
	}

	@Data
	static class AccountCollectionTestDto {
		private List<AccountTestDto> accounts = new ArrayList<>();
	}

	@Data
	static class AccountTestDto {
		private UUID id;
		private Double balance;
		private String currency;
		private Instant created;
		private Instant updated;
	}

	@Data
	static class TransactionTestDto {
		private UUID id;
		private String status;
		private Instant created;
		private String message;
	}
}
