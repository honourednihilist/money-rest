package org.bitbucket.honourednihilist.moneyrest.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;
import org.bitbucket.honourednihilist.moneyrest.dao.AccountMapper;
import org.bitbucket.honourednihilist.moneyrest.model.Account;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.ConcurrentModificationServiceException;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.IllegalArgumentServiceException;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.NotFoundServiceException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.List;

import static java.lang.Thread.sleep;
import static java.util.UUID.fromString;
import static java.util.UUID.randomUUID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

class AccountServiceImplTest {

	private static final Currency JPY = Currency.getInstance("JPY");
	private static final Currency RUB = Currency.getInstance("RUB");
	private static final Currency GBP = Currency.getInstance("GBP");

	private AccountServiceImpl accountService;
	private SqlSessionManager sessionManager;
	private SqlSession session;
	private AccountMapper accountMapper;

	@BeforeEach
	void beforeEach() {
		sessionManager = mock(SqlSessionManager.class);
		session = mock(SqlSession.class);
		accountMapper = mock(AccountMapper.class);

		when(sessionManager.getMapper(AccountMapper.class)).thenReturn(accountMapper);
		when(sessionManager.openSession()).thenReturn(session);
		when(session.getMapper(AccountMapper.class)).thenReturn(accountMapper);

		accountService = new AccountServiceImpl(sessionManager);
	}

	@AfterEach
	void afterEach() {
		reset(sessionManager, session, accountMapper);
	}

	@Test
	void testGetAll() {
		when(accountMapper.getAll()).thenReturn(List.of());
		assertThat(accountService.getAll(), is(emptyCollectionOf(Account.class)));

		var accountA = accountService.create(RUB);
		var accountB = accountService.create(JPY);
		var accountC = accountService.create(GBP);
		when(accountMapper.getAll()).thenReturn(List.of(accountA, accountB, accountC));
		assertThat(accountService.getAll(), containsInAnyOrder(accountC, accountA, accountB));
	}

	@Test
	void testGetOne() {
		var message = assertThrows(IllegalArgumentServiceException.class, () -> accountService.get(null))
				.getMessage();
		assertThat(message, is("id is required"));

		assertThat(accountService.get(randomUUID()), is(nullValue()));

		var accountA = accountService.create(RUB);
		var accountB = accountService.create(JPY);

		when(accountMapper.get(accountA.getId())).thenReturn(accountA);
		when(accountMapper.get(accountB.getId())).thenReturn(accountB);

		assertThat(accountService.get(accountA.getId()), is(accountA));
		assertThat(accountService.get(accountB.getId()), is(accountB));
	}

	@Test
	void testCreate() throws Exception {
		var message = assertThrows(IllegalArgumentServiceException.class, () -> accountService.create(null))
				.getMessage();
		assertThat(message, is("currency is required"));

		var before = Instant.now();
		sleep(100);

		var account = accountService.create(JPY);
		assertThat(account.getId(), is(notNullValue()));
		assertThat(account.getBalance().compareTo(BigDecimal.ZERO), is(0));
		assertThat(account.getCurrency(), is(JPY));
		assertThat(account.getCreated().isAfter(before), is(true));
		assertThat(account.getUpdated(), is(account.getCreated()));
		assertThat(account.getVersion(), is(1L));
		verify(accountMapper).create(account);
	}

	@Test
	void testDeleteNullId() {
		var message = assertThrows(IllegalArgumentServiceException.class, () -> accountService.delete(null))
				.getMessage();
		assertThat(message, is("id is required"));
		verifyZeroInteractions(sessionManager);
	}

	@Test
	void testDeleteNonExistingAccount() {
		var id = fromString("0ea6bb9d-50a5-4a37-add4-cd06595dc435");
		when(accountMapper.get(id)).thenReturn(null);

		var message = assertThrows(NotFoundServiceException.class, () -> accountService.delete(id))
				.getMessage();
		assertThat(message, is("Account [0ea6bb9d-50a5-4a37-add4-cd06595dc435] does not exist"));

		verify(accountMapper, never()).delete(any());
		verify(session, never()).commit();
		verify(session).close();
	}

	@Test
	void testDeleteNonZeroBalanceAccount() {
		var account = accountService.create(JPY);
		account.setId(fromString("f1e14874-202d-4a16-b379-4e8afe4201ac"));
		account.setBalance(BigDecimal.ONE);
		when(accountMapper.get(account.getId())).thenReturn(account);

		var message = assertThrows(IllegalArgumentServiceException.class, () -> accountService.delete(account.getId()))
				.getMessage();
		assertThat(message, is("Account [f1e14874-202d-4a16-b379-4e8afe4201ac] has non-zero balance"));

		verify(accountMapper, never()).delete(any());
		verify(session, never()).commit();
		verify(session).close();
	}

	@Test
	void testDeleteWhenConcurrentModificationHappens() {
		var account = accountService.create(JPY);
		account.setId(fromString("d4cf23ef-602c-41d4-bb8f-0ac41feb94e9"));

		when(accountMapper.get(account.getId())).thenReturn(account);
		when(accountMapper.delete(account)).thenReturn(0);

		var message = assertThrows(ConcurrentModificationServiceException.class, () -> accountService.delete(account.getId()))
				.getMessage();
		assertThat(message, is("Account [d4cf23ef-602c-41d4-bb8f-0ac41feb94e9] has been changed concurrently"));

		verify(session, never()).commit();
		verify(session).close();
	}

	@Test
	void testDeleteOk() throws Exception {
		var account = accountService.create(JPY);
		when(accountMapper.get(account.getId())).thenReturn(account);
		when(accountMapper.delete(account)).thenReturn(1);

		sleep(100);
		accountService.delete(account.getId());
		assertThat(account.getUpdated().isAfter(account.getCreated()), is(true));

		var inOrder = inOrder(session, sessionManager, accountMapper);
		inOrder.verify(sessionManager).openSession();
		inOrder.verify(accountMapper).get(account.getId());
		inOrder.verify(accountMapper).delete(account);
		inOrder.verify(session).commit();
		inOrder.verify(session).close();
	}
}
