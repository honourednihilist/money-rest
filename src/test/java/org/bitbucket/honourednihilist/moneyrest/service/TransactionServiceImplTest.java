package org.bitbucket.honourednihilist.moneyrest.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;
import org.bitbucket.honourednihilist.moneyrest.dao.AccountMapper;
import org.bitbucket.honourednihilist.moneyrest.model.Transaction;
import org.bitbucket.honourednihilist.moneyrest.service.ServiceException.IllegalArgumentServiceException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.List;
import java.util.UUID;

import static java.lang.Thread.sleep;
import static java.util.UUID.fromString;
import static java.util.UUID.randomUUID;
import static org.bitbucket.honourednihilist.moneyrest.model.Transaction.Status.COMPLETED;
import static org.bitbucket.honourednihilist.moneyrest.model.Transaction.Status.DECLINED;
import static org.bitbucket.honourednihilist.moneyrest.model.Transaction.Status.FAILED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

class TransactionServiceImplTest {

	private static final Currency JPY = Currency.getInstance("JPY");
	private static final Currency RUB = Currency.getInstance("RUB");

	private TransactionServiceImpl transactionService;
	private AccountServiceImpl accountService;

	private SqlSessionManager sessionManager;
	private SqlSession session;
	private AccountMapper accountMapper;

	@BeforeEach
	void beforeEach() {
		sessionManager = mock(SqlSessionManager.class);
		session = mock(SqlSession.class);
		accountMapper = mock(AccountMapper.class);

		when(sessionManager.getMapper(AccountMapper.class)).thenReturn(accountMapper);
		when(sessionManager.openSession()).thenReturn(session);
		when(session.getMapper(AccountMapper.class)).thenReturn(accountMapper);

		accountService = new AccountServiceImpl(sessionManager);
		transactionService = new TransactionServiceImpl(sessionManager);
	}

	@AfterEach
	void afterEach() {
		reset(sessionManager, session, accountMapper);
	}

	@Test
	void testCreateTransactionsWhenSourceNull() {
		List<Executable> executables = List.of(
				() -> transactionService.createWithdrawal(null, BigDecimal.TEN, JPY),
				() -> transactionService.createTransfer(null, randomUUID(),BigDecimal.TEN, JPY));

		for (var exec : executables) {
			var message = assertThrows(IllegalArgumentServiceException.class, exec).getMessage();
			assertThat(message, is("source id is required"));
		}

		verifyZeroInteractions(sessionManager);
	}

	@Test
	void testCreateTransactionsWhenTargetNull() {
		List<Executable> executables = List.of(
				() -> transactionService.createDeposit(null, BigDecimal.TEN, JPY),
				() -> transactionService.createTransfer(randomUUID(), null, BigDecimal.TEN, JPY));

		for (var exec : executables) {
			var message = assertThrows(IllegalArgumentServiceException.class, exec).getMessage();
			assertThat(message, is("target id is required"));
		}

		verifyZeroInteractions(sessionManager);
	}

	@Test
	void testCreateTransactionsWhenCurrencyNull() {
		List<Executable> executables = List.of(
				() -> transactionService.createDeposit(randomUUID(), BigDecimal.TEN, null),
				() -> transactionService.createWithdrawal(randomUUID(), BigDecimal.TEN, null),
				() -> transactionService.createTransfer(randomUUID(), randomUUID(), BigDecimal.TEN, null));

		for (var exec : executables) {
			var message = assertThrows(IllegalArgumentServiceException.class, exec).getMessage();
			assertThat(message, is("currency is required"));
		}

		verifyZeroInteractions(sessionManager);
	}

	@Test
	void testCreateTransactionsWhenAmountNull() {
		List<Executable> executables = List.of(
				() -> transactionService.createDeposit(randomUUID(), null, JPY),
				() -> transactionService.createWithdrawal(randomUUID(), null, JPY),
				() -> transactionService.createTransfer(randomUUID(), randomUUID(), null, JPY));

		for (var exec : executables) {
			var message = assertThrows(IllegalArgumentServiceException.class, exec).getMessage();
			assertThat(message, is("amount is required"));
		}

		verifyZeroInteractions(sessionManager);
	}

	@Test
	void testCreateTransactionsWhenAmountZeroOrNegative() {
		List<Executable> executables = List.of(
				() -> transactionService.createDeposit(randomUUID(), BigDecimal.ZERO, JPY),
				() -> transactionService.createDeposit(randomUUID(), BigDecimal.valueOf(-23), JPY),
				() -> transactionService.createWithdrawal(randomUUID(), BigDecimal.ZERO, JPY),
				() -> transactionService.createWithdrawal(randomUUID(), BigDecimal.valueOf(-23), JPY),
				() -> transactionService.createTransfer(randomUUID(), randomUUID(), BigDecimal.ZERO, JPY),
				() -> transactionService.createTransfer(randomUUID(), randomUUID(), BigDecimal.valueOf(-23), JPY));

		for (var exec : executables) {
			var message = assertThrows(IllegalArgumentServiceException.class, exec).getMessage();
			assertThat(message, is("amount must be positive"));
		}

		verifyZeroInteractions(sessionManager);
	}

	@Test
	void testCreateTransactionsWhenAmountTooFractional() {
		List<Executable> executables = List.of(
				() -> transactionService.createDeposit(randomUUID(), BigDecimal.valueOf(0.5), JPY),
				() -> transactionService.createDeposit(randomUUID(), BigDecimal.valueOf(0.005), RUB),
				() -> transactionService.createWithdrawal(randomUUID(), BigDecimal.valueOf(0.5), JPY),
				() -> transactionService.createWithdrawal(randomUUID(), BigDecimal.valueOf(0.005), RUB),
				() -> transactionService.createTransfer(randomUUID(), randomUUID(), BigDecimal.valueOf(0.5), JPY),
				() -> transactionService.createTransfer(randomUUID(), randomUUID(), BigDecimal.valueOf(0.005), RUB));

		for (var exec : executables) {
			var message = assertThrows(IllegalArgumentServiceException.class, exec).getMessage();
			assertThat(message, either(is("Number of fraction digits used with currency [JPY] must be not greater than [0]"))
					.or(is("Number of fraction digits used with currency [RUB] must be not greater than [2]")));
		}

		verifyZeroInteractions(sessionManager);
	}

	@Test
	void testCreateTransactionsWhenAccountNotFound() throws Exception {
		var account = accountService.create(JPY);
		when(accountMapper.get(account.getId())).thenReturn(account);

		UUID id = fromString("b49481de-6b8d-4ecd-8f9d-0bc075b47217");
		List<ThrowingSupplier<Transaction>> suppliers = List.of(
				() -> transactionService.createDeposit(id, BigDecimal.ONE, JPY),
				() -> transactionService.createWithdrawal(id, BigDecimal.ONE, JPY),
				() -> transactionService.createTransfer(id, account.getId(), BigDecimal.ONE, JPY),
				() -> transactionService.createTransfer(account.getId(), id, BigDecimal.ONE, JPY));

		for (var supplier : suppliers) {
			var before = Instant.now();
			sleep(100);

			var transaction = assertDoesNotThrow(supplier);
			assertThat(transaction.getId(), is(notNullValue()));
			assertThat(transaction.getStatus(), is(DECLINED));
			assertThat(transaction.getCreated().isAfter(before), is(true));
			assertThat(transaction.getMessage(), is("Account [b49481de-6b8d-4ecd-8f9d-0bc075b47217] does not exist"));

			var inOrder = inOrder(session, sessionManager, accountMapper);
			inOrder.verify(sessionManager).openSession();
			inOrder.verify(accountMapper, never()).update(any());
			inOrder.verify(session, never()).commit();
			inOrder.verify(session).close();
		}
	}

	@Test
	void testCreateTransactionsWhenAccountNotInCurrency() throws Exception {
		var accountA = accountService.create(JPY);
		accountA.setId(fromString("10ea0474-28a0-4d24-983d-9ac3ff7618bb"));
		var accountB = accountService.create(JPY);
		when(accountMapper.get(accountA.getId())).thenReturn(accountA);
		when(accountMapper.get(accountB.getId())).thenReturn(accountB);

		List<ThrowingSupplier<Transaction>> suppliers = List.of(
				() -> transactionService.createDeposit(accountA.getId(), BigDecimal.ONE, RUB),
				() -> transactionService.createWithdrawal(accountA.getId(), BigDecimal.ONE, RUB),
				() -> transactionService.createTransfer(accountA.getId(), accountB.getId(), BigDecimal.ONE, RUB));

		for (var supplier : suppliers) {
			var before = Instant.now();
			sleep(100);

			var transaction = assertDoesNotThrow(supplier);
			assertThat(transaction.getId(), is(notNullValue()));
			assertThat(transaction.getStatus(), is(DECLINED));
			assertThat(transaction.getCreated().isAfter(before), is(true));
			assertThat(transaction.getMessage(), is("Account [10ea0474-28a0-4d24-983d-9ac3ff7618bb] is in a different currency than [RUB]"));

			var inOrder = inOrder(session, sessionManager, accountMapper);
			inOrder.verify(sessionManager).openSession();
			inOrder.verify(accountMapper, never()).update(any());
			inOrder.verify(session, never()).commit();
			inOrder.verify(session).close();
		}
	}

	@Test
	void testCreateTransferWhenAccountsHaveDifferentCurrencies() throws Exception {
		var accountA = accountService.create(JPY);
		var accountB = accountService.create(RUB);

		accountA.setId(fromString("913736b6-02f0-4f2c-864d-0b667f2801b5"));
		accountB.setId(fromString("198a4345-02e9-4b98-adae-ac8f09839596"));

		when(accountMapper.get(accountA.getId())).thenReturn(accountA);
		when(accountMapper.get(accountB.getId())).thenReturn(accountB);

		var before = Instant.now();
		sleep(100);

		var transaction = assertDoesNotThrow(() -> transactionService.createTransfer(accountA.getId(), accountB.getId(), BigDecimal.ONE, JPY));
		assertThat(transaction.getId(), is(notNullValue()));
		assertThat(transaction.getStatus(), is(DECLINED));
		assertThat(transaction.getCreated().isAfter(before), is(true));
		assertThat(transaction.getMessage(), is("Both source account [913736b6-02f0-4f2c-864d-0b667f2801b5] and " +
				"target account [198a4345-02e9-4b98-adae-ac8f09839596] must be in the same currency [JPY]"));

		var inOrder = inOrder(session, sessionManager, accountMapper);
		inOrder.verify(sessionManager).openSession();
		inOrder.verify(accountMapper, never()).update(any());
		inOrder.verify(session, never()).commit();
		inOrder.verify(session).close();
	}

	@Test
	void testCreateTransferWhenSourceTheSameAsTarget() throws Exception {
		var account = accountService.create(JPY);
		account.setId(fromString("60243384-3e48-448c-884a-c58e5fb4f2d8"));
		when(accountMapper.get(account.getId())).thenReturn(account);

		var before = Instant.now();
		sleep(100);

		var transaction = assertDoesNotThrow(() -> transactionService.createTransfer(account.getId(), account.getId(), BigDecimal.ONE, JPY));
		assertThat(transaction.getId(), is(notNullValue()));
		assertThat(transaction.getStatus(), is(DECLINED));
		assertThat(transaction.getCreated().isAfter(before), is(true));
		assertThat(transaction.getMessage(), is("Both source account and target account are " +
				"actually the same account [60243384-3e48-448c-884a-c58e5fb4f2d8]"));

		var inOrder = inOrder(session, sessionManager, accountMapper);
		inOrder.verify(sessionManager).openSession();
		inOrder.verify(accountMapper, never()).update(any());
		inOrder.verify(session, never()).commit();
		inOrder.verify(session).close();
	}

	@Test
	void testCreateTransactionsWhenNotEnoughMoney() throws Exception {
		var accountA = accountService.create(JPY);
		accountA.setId(fromString("4402b4c9-72c2-4219-9fea-df6d93142fdb"));
		var accountB = accountService.create(JPY);
		when(accountMapper.get(accountA.getId())).thenReturn(accountA);
		when(accountMapper.get(accountB.getId())).thenReturn(accountB);

		List<ThrowingSupplier<Transaction>> suppliers = List.of(
				() -> transactionService.createWithdrawal(accountA.getId(), BigDecimal.ONE, JPY),
				() -> transactionService.createTransfer(accountA.getId(), accountB.getId(), BigDecimal.ONE, JPY));

		for (var supplier : suppliers) {
			var before = Instant.now();
			sleep(100);

			var transaction = assertDoesNotThrow(supplier);
			assertThat(transaction.getId(), is(notNullValue()));
			assertThat(transaction.getStatus(), is(DECLINED));
			assertThat(transaction.getCreated().isAfter(before), is(true));
			assertThat(transaction.getMessage(), is("Account [4402b4c9-72c2-4219-9fea-df6d93142fdb] " +
					"has insufficient balance for the requested operation"));

			var inOrder = inOrder(session, sessionManager, accountMapper);
			inOrder.verify(sessionManager).openSession();
			inOrder.verify(accountMapper, never()).update(any());
			inOrder.verify(session, never()).commit();
			inOrder.verify(session).close();
		}
	}

	@Test
	void testCreateTransactionsWhenConcurrentModificationHappens() throws Exception {
		var accountA = accountService.create(JPY);
		var accountB = accountService.create(JPY);
		var accountC = accountService.create(JPY);

		accountA.setBalance(BigDecimal.TEN);
		accountA.setId(fromString("23fcded4-3e02-4c20-823f-eac27826fbc3"));
		accountB.setBalance(BigDecimal.TEN);
		accountC.setId(fromString("b6120b34-1265-47c4-b971-a8fb9e84b1a9"));

		when(accountMapper.get(accountA.getId())).thenReturn(accountA);
		when(accountMapper.get(accountB.getId())).thenReturn(accountB);
		when(accountMapper.get(accountC.getId())).thenReturn(accountC);

		when(accountMapper.update(accountA)).thenReturn(0);
		when(accountMapper.update(accountB)).thenReturn(1);
		when(accountMapper.update(accountC)).thenReturn(0);

		List<ThrowingSupplier<Transaction>> suppliers = List.of(
				() -> transactionService.createDeposit(accountA.getId(), BigDecimal.ONE, JPY),
				() -> transactionService.createWithdrawal(accountA.getId(), BigDecimal.ONE, JPY),
				() -> transactionService.createTransfer(accountA.getId(), accountB.getId(), BigDecimal.ONE, JPY),
				() -> transactionService.createTransfer(accountB.getId(), accountC.getId(), BigDecimal.ONE, JPY));

		for (var supplier : suppliers) {
			var before = Instant.now();
			sleep(100);

			var transaction = assertDoesNotThrow(supplier);
			assertThat(transaction.getId(), is(notNullValue()));
			assertThat(transaction.getStatus(), is(FAILED));
			assertThat(transaction.getCreated().isAfter(before), is(true));
			assertThat(transaction.getMessage(), either(is("Account [23fcded4-3e02-4c20-823f-eac27826fbc3] has been changed concurrently"))
					.or(is("Account [b6120b34-1265-47c4-b971-a8fb9e84b1a9] has been changed concurrently")));

			var inOrder = inOrder(session, sessionManager, accountMapper);
			inOrder.verify(sessionManager).openSession();
			inOrder.verify(session, never()).commit();
			inOrder.verify(session).close();
		}
	}

	@Test
	void testCreateDepositOk() throws Exception {
		var account = accountService.create(JPY);
		when(accountMapper.get(account.getId())).thenReturn(account);
		when(accountMapper.update(account)).thenReturn(1);

		var before = Instant.now();
		sleep(100);

		var transaction = assertDoesNotThrow(() -> transactionService.createDeposit(account.getId(), BigDecimal.ONE, JPY));
		assertThat(transaction.getId(), is(notNullValue()));
		assertThat(transaction.getStatus(), is(COMPLETED));
		assertThat(transaction.getCreated().isAfter(before), is(true));
		assertThat(transaction.getMessage(), is(nullValue()));

		assertThat(account.getBalance().compareTo(BigDecimal.ONE), is(0));
		assertThat(account.getUpdated().isAfter(before), is(true));

		var inOrder = inOrder(session, sessionManager, accountMapper);
		inOrder.verify(sessionManager).openSession();
		inOrder.verify(accountMapper).get(account.getId());
		inOrder.verify(accountMapper).update(account);
		inOrder.verify(session).commit();
		inOrder.verify(session).close();
	}

	@Test
	void testCreateWithdrawalOk() throws Exception {
		var account = accountService.create(JPY);
		account.setBalance(BigDecimal.TEN);
		when(accountMapper.get(account.getId())).thenReturn(account);
		when(accountMapper.update(account)).thenReturn(1);

		var before = Instant.now();
		sleep(100);

		var transaction = assertDoesNotThrow(() -> transactionService.createWithdrawal(account.getId(), BigDecimal.ONE, JPY));
		assertThat(transaction.getId(), is(notNullValue()));
		assertThat(transaction.getStatus(), is(COMPLETED));
		assertThat(transaction.getCreated().isAfter(before), is(true));
		assertThat(transaction.getMessage(), is(nullValue()));

		assertThat(account.getBalance().compareTo(BigDecimal.valueOf(9L)), is(0));
		assertThat(account.getUpdated().isAfter(before), is(true));

		var inOrder = inOrder(session, sessionManager, accountMapper);
		inOrder.verify(sessionManager).openSession();
		inOrder.verify(accountMapper).get(account.getId());
		inOrder.verify(accountMapper).update(account);
		inOrder.verify(session).commit();
		inOrder.verify(session).close();
	}

	@Test
	void testCreateTransferOk() throws Exception {
		var accountA = accountService.create(JPY);
		var accountB = accountService.create(JPY);

		when(accountMapper.get(accountA.getId())).thenReturn(accountA);
		when(accountMapper.get(accountB.getId())).thenReturn(accountB);

		when(accountMapper.update(accountA)).thenReturn(1);
		when(accountMapper.update(accountB)).thenReturn(1);

		accountA.setBalance(BigDecimal.TEN);

		var before = Instant.now();
		sleep(100);

		var transaction = assertDoesNotThrow(() -> transactionService.createTransfer(accountA.getId(), accountB.getId(), BigDecimal.ONE, JPY));
		assertThat(transaction.getId(), is(notNullValue()));
		assertThat(transaction.getStatus(), is(COMPLETED));
		assertThat(transaction.getCreated().isAfter(before), is(true));
		assertThat(transaction.getMessage(), is(nullValue()));

		assertThat(accountA.getBalance().compareTo(BigDecimal.valueOf(9L)), is(0));
		assertThat(accountA.getUpdated().isAfter(before), is(true));

		assertThat(accountB.getBalance().compareTo(BigDecimal.ONE), is(0));
		assertThat(accountB.getUpdated(), is(accountA.getUpdated()));

		var inOrder = inOrder(session, sessionManager, accountMapper);
		inOrder.verify(sessionManager).openSession();
		inOrder.verify(accountMapper).get(accountA.getId());
		inOrder.verify(accountMapper).get(accountB.getId());
		inOrder.verify(accountMapper).update(accountA);
		inOrder.verify(accountMapper).update(accountB);
		inOrder.verify(session).commit();
		inOrder.verify(session).close();
	}
}
