package org.bitbucket.honourednihilist.moneyrest.dao;

import com.google.inject.Guice;

import org.apache.ibatis.session.SqlSessionManager;
import org.bitbucket.honourednihilist.moneyrest.guice.ConfigModule;
import org.bitbucket.honourednihilist.moneyrest.guice.DbModule;
import org.bitbucket.honourednihilist.moneyrest.model.Account;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Thread.sleep;
import static java.util.UUID.randomUUID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

class AccountMapperTest {

	private static final Currency JPY = Currency.getInstance("JPY");

	private static SqlSessionManager sessionManager;

	@BeforeAll
	static void beforeAll() {
		sessionManager = Guice.createInjector(new ConfigModule(), new DbModule())
				.getInstance(SqlSessionManager.class);
	}

	@Test
	void testConcurrentUpdates() throws Exception {
		var canTransactionOneUpdate = new AtomicBoolean();
		var canTransactionTwoSelect = new AtomicBoolean();

		var initialAccount = createAccount();

		Runnable transactionOne = () -> {
			try (var session = sessionManager.openSession()) {
				var accountMapper = session.getMapper(AccountMapper.class);
				var account = accountMapper.get(initialAccount.getId());
				canTransactionTwoSelect.set(true);

				while (!canTransactionOneUpdate.get()) {
					sleep(100);
				}

				account.setBalance(account.getBalance().add(BigDecimal.ONE));
				account.setUpdated(account.getCreated().plusSeconds(1));

				if (accountMapper.update(account) == 0) {
					return;
				}

				session.commit();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		};

		Runnable transactionTwo = () -> {
			try (var session = sessionManager.openSession()) {
				var accountMapper = session.getMapper(AccountMapper.class);

				while (!canTransactionTwoSelect.get()) {
					sleep(100);
				}

				var account = accountMapper.get(initialAccount.getId());
				account.setBalance(account.getBalance().add(BigDecimal.TEN));
				account.setUpdated(account.getCreated().plusSeconds(10));

				if (accountMapper.update(account) == 0) {
					throw new RuntimeException("Oops, I don't know how this happened");
				}

				session.commit();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}

			canTransactionOneUpdate.set(true);
		};

		var threadOne = new Thread(transactionOne);
		var threadTwo = new Thread(transactionTwo);

		threadOne.start();
		threadTwo.start();
		threadTwo.join();
		threadOne.join();

		var accountAfter = getMapper().get(initialAccount.getId());
		assertThat(accountAfter.getId(), is(initialAccount.getId()));
		assertThat(accountAfter.getBalance().compareTo(BigDecimal.TEN), is(0));
		assertThat(accountAfter.getCurrency(), is(initialAccount.getCurrency()));
		assertThat(accountAfter.getCreated(), is(initialAccount.getCreated()));
		assertThat(accountAfter.getUpdated(), is(initialAccount.getUpdated().plusSeconds(10)));
		assertThat(accountAfter.getVersion(), is(2L));

		getMapper().delete(accountAfter);
		assertThat(getMapper().get(accountAfter.getId()), is(nullValue()));
		assertThat(getMapper().getAll(), is(emptyCollectionOf(Account.class)));
	}

	@Test
	void testConcurrentDeletes() throws Exception {
		var canTransactionOneUpdate = new AtomicBoolean();
		var canTransactionTwoSelect = new AtomicBoolean();

		var initialAccount = createAccount();

		Runnable transactionOne = () -> {
			try (var session = sessionManager.openSession()) {
				var accountMapper = session.getMapper(AccountMapper.class);
				var account = accountMapper.get(initialAccount.getId());
				canTransactionTwoSelect.set(true);

				while (!canTransactionOneUpdate.get()) {
					sleep(100);
				}

				account.setUpdated(account.getCreated().plusSeconds(1));

				if (accountMapper.delete(account) == 0) {
					return;
				}

				session.commit();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		};

		Runnable transactionTwo = () -> {
			try (var session = sessionManager.openSession()) {
				var accountMapper = session.getMapper(AccountMapper.class);

				while (!canTransactionTwoSelect.get()) {
					sleep(100);
				}

				var account = accountMapper.get(initialAccount.getId());
				account.setUpdated(account.getCreated().plusSeconds(10));

				if (accountMapper.delete(account) == 0) {
					throw new RuntimeException("Oops, I don't know how this happened");
				}

				session.commit();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}

			canTransactionOneUpdate.set(true);
		};

		var threadOne = new Thread(transactionOne);
		var threadTwo = new Thread(transactionTwo);

		threadOne.start();
		threadTwo.start();
		threadTwo.join();
		threadOne.join();

		assertThat(getMapper().get(initialAccount.getId()), is(nullValue()));
		assertThat(getMapper().getAll(), is(emptyCollectionOf(Account.class)));

		var accountAfter = getDeletedAccount(initialAccount.getId());
		assertThat(accountAfter.getId(), is(initialAccount.getId()));
		assertThat(accountAfter.getBalance().compareTo(BigDecimal.ZERO), is(0));
		assertThat(accountAfter.getCurrency(), is(initialAccount.getCurrency()));
		assertThat(accountAfter.getCreated(), is(initialAccount.getCreated()));
		assertThat(accountAfter.getUpdated(), is(initialAccount.getUpdated().plusSeconds(10)));
		assertThat(accountAfter.getVersion(), is(2L));
	}

	@Test
	void testConcurrentUpdateAndDelete() throws Exception {
		var canTransactionOneUpdate = new AtomicBoolean();
		var canTransactionTwoSelect = new AtomicBoolean();

		var initialAccount = createAccount();

		Runnable transactionOne = () -> {
			try (var session = sessionManager.openSession()) {
				var accountMapper = session.getMapper(AccountMapper.class);
				var account = accountMapper.get(initialAccount.getId());
				canTransactionTwoSelect.set(true);

				while (!canTransactionOneUpdate.get()) {
					sleep(100);
				}

				account.setBalance(account.getBalance().add(BigDecimal.ONE));
				account.setUpdated(account.getCreated().plusSeconds(1));

				if (accountMapper.update(account) == 0) {
					return;
				}

				session.commit();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		};

		Runnable transactionTwo = () -> {
			try (var session = sessionManager.openSession()) {
				var accountMapper = session.getMapper(AccountMapper.class);

				while (!canTransactionTwoSelect.get()) {
					sleep(100);
				}

				var account = accountMapper.get(initialAccount.getId());
				account.setUpdated(account.getCreated().plusSeconds(10));

				if (accountMapper.delete(account) == 0) {
					throw new RuntimeException("Oops, I don't know how this happened");
				}

				session.commit();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}

			canTransactionOneUpdate.set(true);
		};

		var threadOne = new Thread(transactionOne);
		var threadTwo = new Thread(transactionTwo);

		threadOne.start();
		threadTwo.start();
		threadTwo.join();
		threadOne.join();

		assertThat(getMapper().get(initialAccount.getId()), is(nullValue()));
		assertThat(getMapper().getAll(), is(emptyCollectionOf(Account.class)));

		var accountAfter = getDeletedAccount(initialAccount.getId());
		assertThat(accountAfter.getId(), is(initialAccount.getId()));
		assertThat(accountAfter.getBalance().compareTo(BigDecimal.ZERO), is(0));
		assertThat(accountAfter.getCurrency(), is(initialAccount.getCurrency()));
		assertThat(accountAfter.getCreated(), is(initialAccount.getCreated()));
		assertThat(accountAfter.getUpdated(), is(initialAccount.getUpdated().plusSeconds(10)));
		assertThat(accountAfter.getVersion(), is(2L));
	}

	@Test
	void testConcurrentDeleteAndUpdate() throws Exception {
		var canTransactionOneUpdate = new AtomicBoolean();
		var canTransactionTwoSelect = new AtomicBoolean();

		var initialAccount = createAccount();

		Runnable transactionOne = () -> {
			try (var session = sessionManager.openSession()) {
				var accountMapper = session.getMapper(AccountMapper.class);
				var account = accountMapper.get(initialAccount.getId());
				canTransactionTwoSelect.set(true);

				while (!canTransactionOneUpdate.get()) {
					sleep(100);
				}

				account.setUpdated(account.getCreated().plusSeconds(1));

				if (accountMapper.delete(account) == 0) {
					return;
				}

				session.commit();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		};

		Runnable transactionTwo = () -> {
			try (var session = sessionManager.openSession()) {
				var accountMapper = session.getMapper(AccountMapper.class);

				while (!canTransactionTwoSelect.get()) {
					sleep(100);
				}

				var account = accountMapper.get(initialAccount.getId());
				account.setBalance(account.getBalance().add(BigDecimal.TEN));
				account.setUpdated(account.getCreated().plusSeconds(10));

				if (accountMapper.update(account) == 0) {
					throw new RuntimeException("Oops, I don't know how this happened");
				}

				session.commit();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}

			canTransactionOneUpdate.set(true);
		};

		var threadOne = new Thread(transactionOne);
		var threadTwo = new Thread(transactionTwo);

		threadOne.start();
		threadTwo.start();
		threadTwo.join();
		threadOne.join();

		var accountAfter = getMapper().get(initialAccount.getId());
		assertThat(accountAfter.getId(), is(initialAccount.getId()));
		assertThat(accountAfter.getBalance().compareTo(BigDecimal.TEN), is(0));
		assertThat(accountAfter.getCurrency(), is(initialAccount.getCurrency()));
		assertThat(accountAfter.getCreated(), is(initialAccount.getCreated()));
		assertThat(accountAfter.getUpdated(), is(initialAccount.getUpdated().plusSeconds(10)));
		assertThat(accountAfter.getVersion(), is(2L));

		getMapper().delete(accountAfter);
		assertThat(getMapper().get(accountAfter.getId()), is(nullValue()));
		assertThat(getMapper().getAll(), is(emptyCollectionOf(Account.class)));
	}

	@Test
	void testNotDeleteOrUpdateDeletedAccounts() throws Exception {
		var account = createAccount();
		getMapper().delete(account);

		account.setUpdated(account.getCreated().plusSeconds(17));
		assertThat(getMapper().update(account), is(0));

		account.setUpdated(account.getCreated().plusSeconds(17));
		assertThat(getMapper().delete(account), is(0));

		var deletedAccount = getDeletedAccount(account.getId());
		assertThat(deletedAccount.getId(), is(account.getId()));
		assertThat(deletedAccount.getBalance().compareTo(account.getBalance()), is(0));
		assertThat(deletedAccount.getCurrency(), is(account.getCurrency()));
		assertThat(deletedAccount.getCreated(), is(account.getCreated()));
		assertThat(deletedAccount.getUpdated(), is(account.getCreated()));
		assertThat(deletedAccount.getVersion(), is(2L));
	}

	@Test
	void testVersionIncrement() throws Exception {
		var account = createAccount();

		for (int i = 0; i < 10; i++) {
			int update = getMapper().update(account);
			assertThat(update, is(1));

			update = getMapper().update(account);
			assertThat(update, is(0));

			account = getMapper().get(account.getId());
			assertThat(account.getVersion(), is(i + 2L));
		}

		int delete = getMapper().delete(account);
		assertThat(delete, is(1));

		delete = getMapper().delete(account);
		assertThat(delete, is(0));

		var deletedAccount = getDeletedAccount(account.getId());
		assertThat(deletedAccount.getVersion(), is(12L));
	}

	private static Account createAccount() {
		var account = new Account();
		account.setId(randomUUID());
		account.setBalance(BigDecimal.ZERO);
		account.setCurrency(JPY);
		account.setCreated(Instant.now());
		account.setUpdated(account.getCreated());
		account.setVersion(1);

		getMapper().create(account);
		return account;
	}

	private static AccountMapper getMapper() {
		return sessionManager.getMapper(AccountMapper.class);
	}

	private static Account getDeletedAccount(UUID id) throws Exception {
		String sql = "SELECT id, balance, currency, created, updated, version FROM accounts WHERE id = ? AND deleted IS TRUE";

		try (var session = sessionManager.openSession();
		     var statement = session.getConnection().prepareStatement(sql)) {

			statement.setString(1, id.toString());
			var resultSet = statement.executeQuery();
			assertThat(resultSet.next(), is(true));

			var account = new Account();
			account.setId(UUID.fromString(resultSet.getString("id")));
			account.setBalance(resultSet.getBigDecimal("balance"));
			account.setCurrency(Currency.getInstance(resultSet.getString("currency")));
			account.setCreated(resultSet.getTimestamp("created").toInstant());
			account.setUpdated(resultSet.getTimestamp("updated").toInstant());
			account.setVersion(resultSet.getLong("version"));

			assertThat(resultSet.next(), is(false));

			return account;
		}
	}
}
